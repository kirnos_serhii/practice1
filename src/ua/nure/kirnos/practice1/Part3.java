package ua.nure.kirnos.practice1;

/**
 * Class that implements the functionality of displaying command line parameters
 * in the console.
 *
 * @author Kirnos Serhii
 */
public final class Part3 {

	/**
	 * Utility class constructor.
	 */
	private Part3() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Display command line arguments in console.
	 *
	 * @param args - command line arguments
	 */
	public static void displayParameters(final String[] args) {
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				System.out.print(args[i] + ' ');
			}
		} else {
			System.out.print("Length = 0.");
		}
		System.out.println();
	}
	
	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		displayParameters(args);
	}

}
