package ua.nure.kirnos.practice1;

/**
 * Class that implements the functionality of determining the greatest common
 * divisor.
 *
 * @author Kirnos Serhii
 */
public final class Part4 {

	/**
	 * Utility class constructor.
	 */
	private Part4() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Method that implements the functionality of determining the greatest common
	 * divisor.
	 *
	 * @param in1 - first number
	 * @param in2 - second number
	 * @return - greatest common divisor
	 */
	public static int greatestCommonDivisor(final int in1, final int in2) {
		int i1 = in1;
		int i2 = in2;
		int tmp = 0;
		if (i1 < i2) {
			tmp = i1;
			i1 = i2;
			i2 = tmp;
		}
		while (i1 > i2) {
			i1 = i1 - i2;
			if (i1 < i2) {
				tmp = i1;
				i1 = i2;
				i2 = tmp;
			}
		}
		return i1;
	}

	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		if (args.length >= 2) {
			int i1 = Integer.parseInt(args[0]);
			int i2 = Integer.parseInt(args[1]);
			int result = greatestCommonDivisor(i1, i2);
			System.out.println(result);
		}
	}

}
