package ua.nure.kirnos.practice1;

/**
 * Class implements converts numbers base-10 to base-26 system.
 *
 * @author Kirnos Serhii
 */
public final class Part7 {

	/**
	 * Utility class constructor.
	 */
	private Part7() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		String symb = " ==> ";
		System.out.println("A ==> " + str2int("A") + symb + int2str(str2int("A")));
		System.out.println("B ==> " + str2int("B") + symb + int2str(str2int("B")));
		System.out.println("Z ==> " + str2int("Z") + symb + int2str(str2int("Z")));
		System.out.println("AA ==> " + str2int("AA") + symb + int2str(str2int("AA")));
		System.out.println("AZ ==> " + str2int("AZ") + symb + int2str(str2int("AZ")));
		System.out.println("BA ==> " + str2int("BA") + symb + int2str(str2int("BA")));
		System.out.println("ZZ ==> " + str2int("ZZ") + symb + int2str(str2int("ZZ")));
		System.out.println("AAA ==> " + str2int("AAA") + symb + int2str(str2int("AAA")));
	}

	/**
	 * Method of determining the serial number of a column by its letter number.
	 *
	 * @param number - Initial data (letter number)
	 * @return integer number
	 */

	public static int str2int(final String number) {
		int result = 0;
		for (int i = 0; i < number.length(); i++) {
			int c = number.charAt(i);
			c = c - 64;
			result += c * (int) Math.pow(26, ((number.length() - 1) - i));
		}
		return result;
	}

	/**
	 * Method for determining the letter number of a column by its ordinal number.
	 *
	 * @param in - Initial data (integer number)
	 * @return letter number
	 */
	public static String int2str(final int in) {
		int number = in;
		StringBuilder num = new StringBuilder();
		int remainder;
		while (number != 0) {
			remainder = number % 26;
			if (remainder == 0) {
				num.append("Z");
				number -= 26;
			} else {
				num.append((char) (remainder + 64));
			}
			number = number / 26;
		}
		num.reverse();
		return num.toString();
	}

	/**
	 * Method that returns right number.
	 *
	 * @param number - letter number
	 * @return right letter number
	 */
	public static String rightColumn(final String number) {
		return int2str(str2int(number) + 1);
	}

}
