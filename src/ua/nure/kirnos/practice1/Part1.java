package ua.nure.kirnos.practice1;

/**
 * Class displays "Hello, World" in console.
 *
 * @author Kirnos Serhii
 */
public final class Part1 {
	/**
	 * Utility class constructor.
	 */
	private Part1() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Print message "Hello, World".
	 */
	public static void hello() {
		System.out.println("Hello, World");
	}

	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		hello();
	}

}
