package ua.nure.kirnos.practice1;

/**
 * Class that implements the functionality of determining the sum of digits of a
 * positive integer.
 *
 * @author Kirnos Serhii
 */
public final class Part5 {

	/**
	 * Utility class constructor.
	 */
	private Part5() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Method that implements the functionality of determining the sum of digits of
	 * a positive integer.
	 *
	 * @param args - numbers
	 * @return sum digit
	 */
	public static int sumOfDigits(final String[] args) {
		int sum = 0;
		if (args.length >= 1) {
			String str = args[0];
			for (int i = 0; i < str.length(); i++) {
				sum += Integer.parseInt(Character.toString(args[0].charAt(i)));
			}
		}
		return sum;
	}

	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		System.out.println(sumOfDigits(args));
	}

}
