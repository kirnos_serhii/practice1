package ua.nure.kirnos.practice1;

/**
 * Class that creates an array of n elements and fills it with an ascending row
 * of prime numbers.
 *
 * @author Kirnos Serhii
 */
public final class Part6 {

	/**
	 * Utility class constructor.
	 */
	private Part6() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Method that fill array.
	 *
	 * @param args - amount of elements array
	 * @return array of prime numbers
	 */
	public static int[] arrayPrimeNumbers(final String[] args) {
		if (args.length < 1) {
			return new int[0];
		}
		int n = Integer.parseInt(args[0]);
		if (n < 1) {
			return new int[0];
		}
		int[] arr = new int[n];
		int i = 0;
		int pr = 0;
		while (i < n) {
			pr++;
			int count = 0;
			for (int j = 1; j <= pr; j++) {
				if (pr % j == 0) {
					count++;
				}
			}
			if (count == 2) {
				arr[i++] = pr;
			}
		}
		return arr;
	}

	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		int[] arr = arrayPrimeNumbers(args);
		if (arr.length != 0) {
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		} else {
			System.out.println("Argument is empty or < 1.");
		}
	}

}
