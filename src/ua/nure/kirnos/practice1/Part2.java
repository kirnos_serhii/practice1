package ua.nure.kirnos.practice1;

/**
 * Class embody function for performs the addition of two numbers.
 *
 * @author Kirnos Serhii
 */
public final class Part2 {

	/**
	 * Utility class constructor.
	 */
	private Part2() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Addition of two numbers.
	 *
	 * @param args - two nambers
	 * @return sum of numbers
	 */
	public static int sumTwoNumber(final String[] args) {
		int result = 0;
		int i1;
		int i2;
		if (args.length >= 2) {
			i1 = Integer.parseInt(args[0]);
			i2 = Integer.parseInt(args[1]);
			result = i1 + i2;
		}
		return result;
	}

	/**
	 * Demonstration method.
	 *
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		System.out.println(sumTwoNumber(args));
	}

}
