package ua.nure.kirnos.practice1;

/**
 * Class demonstrates the work of all subtasks.
 * @author Kirnos Serhii
 */
public final class Demo {
	/**
	 * Utility class constructor.
	 */
	private Demo() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Method for demonstration task.
	 * @param args - command line arguments
	 */
	public static void main(final String[] args) {
		Part1.main(null);
		Part2.main(new String[] {"81", "36"});
		Part3.main(new String[] {"1", "36", "dfhgdg"});
		Part4.main(new String[] {"81", "36"});
		Part5.main(new String[] {"0"});
		Part6.main(new String[] {"10"});
		System.out.println("Task7:");
		System.out.println("str2int: YZA = " + Part7.str2int("YZA"));
		System.out.println("int2str: 17577 = " + Part7.int2str(17577));
		System.out.println("rightColumn: YZA = " + Part7.rightColumn("YZA"));
	}

}
